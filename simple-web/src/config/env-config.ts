import * as dotenv from 'dotenv';
  dotenv.config();

  export default {
    SIMPLE_API_V1_URL: process.env.REACT_APP_SIMPLE_API_V1_URL,
}
