import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Route';

import Paths from '../constants/paths.constant';

import Home from '../pages/Home';
import Users from '../pages/Users';
import CreateUser from '../pages/Users/Create';
import EditUser from '../pages/Users/Edit';

const Routes: React.FC = () => (
  <Switch>
    <Route path={Paths.Home} exact component={Home} />
    <Route exact path={Paths.User.List} component={Users} />
    <Route exact path={Paths.User.Add} component={CreateUser} />
    <Route exact path={Paths.User.Edit} component={EditUser} />
  </Switch>
);

export default Routes;
