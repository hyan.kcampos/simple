export interface UserTable {
  key: string;
  id: string;
  name: string;
  document: string;
  user: string;
  address: string;
}
