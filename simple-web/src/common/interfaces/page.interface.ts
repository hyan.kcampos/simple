export interface Page {
  page: string;
  handleSetPage(data: string): void;
}
