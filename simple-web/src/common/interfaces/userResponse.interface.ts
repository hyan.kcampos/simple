export interface UserResponse {
  name: string;
  document: string;
  user: {
    login: string;
    password?: string;
  }
  address: {
    cep: string;
    street: string;
    district: string;
    city: string;
    number: string;
  }
}
