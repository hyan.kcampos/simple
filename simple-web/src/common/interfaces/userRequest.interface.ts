export interface UserRequest {
  name: string;
  document: string;
  login: string;
  password?: string;
  cep: string;
  street: string;
  district: string;
  city: string;
  number: string;
}

export interface UserRequestAll {
  id: string;
  name: string;
  document: string;
  login: string;
  password?: string;
  cep: string;
  street: string;
  district: string;
  city: string;
  number: string;
  created_at: string;
  updated_at: string;
}
