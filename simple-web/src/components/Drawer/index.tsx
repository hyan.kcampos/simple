import React, { useContext } from 'react';
import { IoPersonOutline, IoHomeOutline } from 'react-icons/io5';
import { Row, Col } from 'antd';
import { Content, ContentWeb, ContentMobile } from './styles';
import DrawerOption from './Option';
import Header from '../Header';
import Strip from '../Strip';
import PageContext from '../../context/PageContext';
import Paths from '../../constants/paths.constant';

const Drawer: React.FC = ({ children }) => {
  const { page } = useContext(PageContext);
  const iconHome = <IoHomeOutline />;
  const iconUser = <IoPersonOutline />;

  return (
    <>
      <Header />
      <Strip />
      <ContentWeb>
        <Row style={{ height: '100vh' }}>
          <Col>
            <Content>
              <DrawerOption
                name="Home"
                icon={iconHome}
                page={Paths.Home}
                active={page === 'Home'}
              />
              <DrawerOption
                name="Usuários"
                icon={iconUser}
                page={Paths.User.List}
                active={page === 'Usuários'}
              />
            </Content>
          </Col>
          <Col xl={19} xs={17} md={18} xxl={20}>
            {children}
          </Col>
        </Row>
      </ContentWeb>
      <ContentMobile>{children}</ContentMobile>
    </>
  );
};

export default Drawer;
