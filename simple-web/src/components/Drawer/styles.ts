import styled from 'styled-components';
import Color from '../../styles/color';

export const Content = styled.div`
  width: 240px;
  height: 100%;
  background: ${Color.white};
  padding-top: 25px;
`;

export const ContentWeb = styled.div`
  display: block;

  @media (max-width: 1000px) {
    display: none;
  }
`;

export const ContentMobile = styled.div`
  display: none;

  @media (max-width: 1000px) {
    display: block;
  }
`;
