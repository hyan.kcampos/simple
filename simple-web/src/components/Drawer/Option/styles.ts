import styled, { css } from 'styled-components';
import Color from '../../../styles/color';

interface ActiveProps {
  active: boolean;
}

export const Content = styled.button<ActiveProps>`
  padding: 15px;
  display: flex;
  justify-content: start;
  align-items: center;
  width: 100%;
  border: none;
  background: ${Color.white};

  &:hover {
    ${props =>
      !props.active &&
      css`
        background: ${Color.primary.light};
      `};
  }

  ${props =>
    props.active &&
    css`
      background: ${Color.primary.default};
    `};

  svg {
    font-size: 22px;
  }

  p {
    padding-left: 20px;
    margin: 0;
    font-size: 16px;
    color: ${Color.primary.default};

    ${props =>
      props.active &&
      css`
        color: ${Color.white};
      `};
  }
`;
