import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { Content } from './styles';
import PageContext from '../../../context/PageContext';

interface OptionProps {
  name: string;
  icon: React.ReactNode;
  page: string;
  active: boolean;
}

const DrawerOption: React.FC<OptionProps> = ({ name, icon, page, active }) => {
  const { handleSetPage } = useContext(PageContext);
  return (
    <Link to={page}>
      <Content onClick={() => handleSetPage(name)} active={active}>
        <p>{icon}</p>
        <p>{name}</p>
      </Content>
    </Link>
  );
};

export default DrawerOption;
