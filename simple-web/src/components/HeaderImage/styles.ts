import styled from 'styled-components';
import img from '../../assets/logos/logo.png';

export const Content = styled.div`
  width: 100px;
  height: 100px;
  background-image: url(${img});
  background-size: contain;
  background-repeat: no-repeat;
`;
