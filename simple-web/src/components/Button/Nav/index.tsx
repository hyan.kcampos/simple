import React, { useContext } from 'react';
import { Content, Container } from './styles';
import PageContext from '../../../context/PageContext';
import { useHistory } from 'react-router-dom';
import Paths from '../../../constants/paths.constant'

interface ButtonNavProps {
  active: boolean;
  name: string;
}

const ButtonNav: React.FC<ButtonNavProps> = ({ active, name, ...rest }) => {
  const { handleSetPage } = useContext(PageContext);
  const history = useHistory();

  return (
    <Container active={active}>
      <Content
        active={active}
        type="button"
        data-testid="button-nav"
        {...rest}
        onClick={() => {
          handleSetPage(name);

          if (name === "Home") {
            history.push(Paths.Home);
          } else {
            history.push(Paths.User.List);
          }
        }}
      />
    </Container>
  );
};

export default ButtonNav;
