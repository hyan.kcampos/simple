import styled, { css } from 'styled-components';
import Color from '../../../styles/color';

interface ButtonNavProps {
  active: boolean;
}

export const Container = styled.div<ButtonNavProps>`
  height: 100px;
  width: 100px;
  display: flex;
  justify-content: center;
  z-index: 1;

  ${props =>
    props.active &&
    css`
      background: ${Color.primary.default};
    `};
`;

export const Content = styled.button<ButtonNavProps>`
  background: transparent;
  border: none;
  color: ${props => (props.active ? Color.white : Color.text)};
  font-size: 16px;
  font-family: 'Arial';
  display: flex;
  justify-content: center;
  align-items: center;

  :hover {
    opacity: 0.8;
  }
`;
