import styled from 'styled-components';

interface ButtonOutlineProps {
  border?: string;
  color?: string;
}

export const Content = styled.button<ButtonOutlineProps>`
  background: transparent;
  border: none;
  color: ${props => props.color};
  padding: 12px;
  font-size: 16px;
  font-family: 'Arial';
  display: flex;
  justify-content: center;
  align-items: center;

  :hover {
    opacity: 0.8;
  }
`;
