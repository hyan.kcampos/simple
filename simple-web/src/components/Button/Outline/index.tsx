import { ButtonHTMLType } from 'antd/lib/button/button';
import React from 'react';
import { Content } from './styles';

interface ButtonOutlineProps {
  border: string;
  color: string;
  type?: ButtonHTMLType;
}

const ButtonOutline: React.FC<ButtonOutlineProps> = ({
  color,
  border,
  type,
  ...rest
}) => {
  return (
    <Content type={type || 'button'} color={color} border={border} {...rest} />
  );
};

export default ButtonOutline;
