import { ButtonHTMLType } from 'antd/lib/button/button';
import React from 'react';
import { Content } from './styles';

interface ButtonProps {
  background: string;
  color: string;
  type?: ButtonHTMLType;
}

const Button: React.FC<ButtonProps> = ({
  color,
  background,
  type,
  ...rest
}) => {
  return (
    <Content
      type={type || 'button'}
      color={color}
      background={background}
      {...rest}
    />
  );
};

export default Button;
