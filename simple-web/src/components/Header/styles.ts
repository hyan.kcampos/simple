import styled from 'styled-components';
import Color from '../../styles/color'

interface PageProps {
  page: string;
}

export const Content = styled.div`
  width: 100%;
  height: 90px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${Color.white};
  border-bottom: 1px solid #efefef;
  margin: 0 auto;


  div {
    display: flex;

    &:nth-child(1) {
      margin-left: 20px;
    }

    p {
      margin: 0;
      &:nth-child(1) {
        padding-right: 5px;
      }

      &:nth-child(2) {
        padding-right: 5px;
        color: ${Color.primary.default};;
      }
    }

    @media (max-width: 900px) {
      &:nth-child(3) {
        display: none;
      }
    }
  }
`;

export const NavMobile = styled.div<PageProps>`
  display: none;
  max-width: 1440px;

  div {
    @media (min-width: 900px) {
      display: none;
    }
    button {
      display: none;

      @media (max-width: 900px) {
        display: block;
        padding: 10px;
      }
    }
  }
`;
