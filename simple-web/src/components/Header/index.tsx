import React, { useContext } from 'react';
import { Content, NavMobile } from './styles';
import HeaderImage from '../HeaderImage';
import ButtonNav from '../Button/Nav';
import PageContext from '../../context/PageContext';

const Header: React.FC = () => {
  const { page } = useContext(PageContext);
  return (
    <Content>
      <div>
        <HeaderImage />
      </div>
      <NavMobile page={page}>
        <ButtonNav active={page === 'Home'} name="Home">
          Home
        </ButtonNav>
        <ButtonNav active={page === 'Usuários'} name="Usuários">
          Usuários
        </ButtonNav>
      </NavMobile>
      <div>
        <p>Bem vindo,</p>

        <p>Administrador</p>
      </div>
    </Content>
  );
};

export default Header;
