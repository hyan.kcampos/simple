import React, { useCallback } from 'react';
import { Layout, Table, Space, Modal } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import { UserTable } from '../../common/interfaces/userTable.interface';
import { useUser } from '../../hooks/useUser';
import './styles.css';
import Button from '../Button';
import Paths from '../../constants/paths.constant';
import getWindowDimensions from '../../utils/getWindowDimensions';
import Color from '../../styles/color';

interface TableProps {
  users: UserTable[]
}

const TableData: React.FC<TableProps> = ({ users }) => {
  const history = useHistory();
  const { deleteUser } = useUser();
  const { Content } = Layout;
  const { Column } = Table;
  const { confirm } = Modal;
  const { width } = getWindowDimensions();

  const handleMoveEdit = useCallback((id: string) => {
    history.push(Paths.User.Edit.replace(':id', id));
  }, [history]);

  const handleDeleteItem = useCallback((id: string) => {
    deleteUser(id);
  }, [deleteUser]);


  const handleModalShow = useCallback((id: string) => {
      confirm({
        title: 'Deletar este usuário?',
        content: 'Após realizar está ação não será possivel reverte-lá',
        onOk() {
          handleDeleteItem(id);
        },
        onCancel() {
        },
      });
  }, [handleDeleteItem, confirm]);

  return (
    <Layout style={{ margin: '5px 0px 0px 0px', padding: 10}}>
      <Content>
        <div className="container">
          <p className="title">Lista de usuários</p>
          <div className="content-button">
            <Link to={Paths.User.Add}>
              <Button color={Color.white} background={Color.primary.default}>
                Criar usuário
              </Button>
            </Link>
          </div>
        </div>
        <Space wrap />
        <Table dataSource={users}>
          <Column title="ID" dataIndex="id" key="id" />
          {width > 400 && <Column title="Nome" dataIndex="name" key="name" />}
          <Column title="Documento" dataIndex="document" key="document" />
          {width > 650 && <Column title="Usuário" dataIndex="user" key="login" />}
          {width > 650 && <Column title="Endereço" dataIndex="address" key="address" />}
          <Column
            title="Ação"
            key="action"
            render={(record : UserTable) => (
              <Space size="middle">
                <span className='button-table' data-testid="button-edit" onClick={() => handleMoveEdit(record.id)}>Editar</span>
                <span className='button-table' data-testid="button-delete" onClick={() => handleModalShow(record.id)}>Deletar</span>
              </Space>
            )}
          />
          </Table>
      </Content>
    </Layout>
  );
};

export default TableData;
