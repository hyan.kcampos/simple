import { UserRequestAll } from '../common/interfaces/userRequest.interface'
import { User } from '../api/simple-api/user/interfaces/user.interface'

export default function mountDataResponse(data: User): UserRequestAll {
    return {
      id: data.id,
      name: data.name,
      document: data.document,
      login: data.user.login,
      password: data.user.password,
      cep: data.address.cep,
      street: data.address.street,
      district: data.address.district,
      city: data.address.city,
      number: data.address.number,
      created_at: data.created_at,
      updated_at: data.updated_at
    }
}
