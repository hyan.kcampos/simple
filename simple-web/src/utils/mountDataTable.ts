import { UserTable } from '../common/interfaces/userTable.interface'
import { User } from '../api/simple-api/user/interfaces/user.interface'

export default function mountDataTable(data: User[]): UserTable[] {
  let response : UserTable[] = [];
  data.map((user) => {
    response.push({
      key: user.id,
      id: user.id,
      name: user.name,
      document: user.document,
      user: user.user.login,
      address: `
        ${user.address.street ? user.address.street + ',' : ""}
        ${user.address.district ? user.address.district + ', ' : ''}
        ${user.address.number ? user.address.number + ', ' : ''}
        ${user.address.city ? user.address.city + ' - ' : ''}
        ${user.address.cep ? user.address.cep : ''}`
    })
  });

  return response;
}
