import { UserRequest } from '../common/interfaces/userRequest.interface'
import { UserResponse } from '../common/interfaces/userResponse.interface'

export default function mountDataRequest(data: UserRequest): UserResponse{
    return {
      name: data.name,
      document: data.document,
      user: {
        login: data.login,
        password: data.password ? data.password : '',
      },
      address: {
        cep: data.cep,
        street: data.street,
        district: data.district,
        city: data.city,
        number: data.number,
      },
    }
}
