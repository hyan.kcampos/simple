import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import GlobalStyle from './styles/global';
import Routes from './routes';
import 'antd/dist/antd.css';
import Drawer from './components/Drawer';
import AppProvider from './hooks';

const App: React.FC = () => {
  return (
    <Router>
      <AppProvider>
        <Drawer>
          <Routes />
        </Drawer>
      </AppProvider>
      <GlobalStyle />
    </Router>
  );
};

export default App;
