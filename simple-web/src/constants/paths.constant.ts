const Paths = {
  Home: '/',
  User: {
    List: '/users',
    Add: '/users/add',
    Edit: '/users/:id/edit',
  },
};

export default Paths;
