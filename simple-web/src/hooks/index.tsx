import React from 'react';

import { ToastProvider } from './useToast';
import { PageProvider } from '../context/PageContext';
const AppProvider: React.FC = ({ children }) => (
  <PageProvider>
    <ToastProvider>{children}</ToastProvider>
  </PageProvider>
);

export default AppProvider;
