import { useState, useCallback } from 'react';
import { User } from '../api/simple-api/user/interfaces/user.interface';
import { UserCreateRequest } from '../api/simple-api/user/interfaces/userCreateRequest.interface';
import { UserTable } from '../common/interfaces/userTable.interface'
import { UserRequest } from '../common/interfaces/userRequest.interface'
import { useHistory } from 'react-router-dom';
import { useToast } from './useToast';
import { getAllUser, postUser, getUserById, patchUserById, deleteUserById } from '../api/simple-api/user/user.request';
import mountDataTable from '../utils/mountDataTable'
import mountDataRequest from '../utils/mountDataRequest'
import mountDataResponse from '../utils/mountDataResponse'
import Paths from '../constants/paths.constant'

interface useUserReturn {
  findUsers(): Promise<void>;
  createUser(user: UserCreateRequest): Promise<void>;
  findOneUser(id: string): Promise<void>;
  updateUser(id: string, user: Omit<UserRequest, 'id, active, createdAt, updatedAt'>): Promise<void>;
  deleteUser(id: string): Promise<void>;
  loading: boolean;
  error: boolean;
  users: User[];
  user: UserRequest;
  usersTable: UserTable[];
}

export const useUser = (): useUserReturn => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [users, setUsers] = useState<User[]>([]);
  const [user, setUser] = useState<UserRequest>({} as UserRequest);
  const [usersTable, setUsersTable] = useState<UserTable[]>([]);

  const history = useHistory();
  const { addToast } = useToast();

  const findUsers = useCallback(async () => {
      try {
        setError(false);

        const response = await getAllUser();

        if (response.data.records.length) {
          const data = await mountDataTable(response.data.records)
          setUsersTable(data);
        }

        setUsers(response.data.records);
      } catch (err) {
        setError(true);
        setUsers([]);
      } finally {
        setLoading(false);
      }
    },
    [],
  );

  const createUser = useCallback(async (user: UserCreateRequest) => {
      try {
        setError(false);
        setLoading(true);

        await postUser(user);

        addToast({
          type: 'success',
          title: 'Cadastro realizado',
          description: 'Seu usuário foi cadastrado com sucesso!',
        });

        history.push(Paths.User.List);

      } catch (err) {
        setError(true);

        if (err.response.data.status === 409 && err.response.data.data.field === "document") {
          addToast({
            type: 'error',
            title: 'CPF já existe',
            description: 'Este cpf já existe em nossa base, utilize outro para realizar o cadastro!',
          });
        } else if (err.response.data.status === 409 && err.response.data.data.field === "login") {
          addToast({
            type: 'error',
            title: 'Login já existe',
            description: 'Este login já existe em nossa base, utilize outro para realizar o cadastro!',
          });
        } else {
          addToast({
            type: 'error',
            title: 'Erro no cadastro',
            description: 'Ocorreu um erro no cadastro, tente novamente mais tarde!',
          });
        }
        setError(true);
      } finally {
        setLoading(false);
      }
    },
    [addToast, history],
  );

  const findOneUser = useCallback(async (id: string) => {
    try {
      setError(false);
      setLoading(true);

      const response = await getUserById(id);

      if (response.data.records) {
        const data = await mountDataResponse(response.data.records)
        setUser(data);
      }
    } catch (err) {
      if (err.response.data.status === 404) {
        addToast({
          type: 'error',
          title: 'Usuário não existe',
          description: 'Este usuário não existe em nossa base!',
        });
        history.push(Paths.User.List);
      } else {
        addToast({
          type: 'error',
          title: 'Ocorreu um problema',
          description: 'Verifique sua conexão e tente novamente!',
        });
      }
      setError(true);
    } finally {
      setLoading(false);
    }
  }, [addToast, history]);

  const updateUser = useCallback(
    async function (id: string,
      data: Omit<UserRequest, 'id, active, createdAt, updatedAt'>) {
      try {
        setLoading(true);
        const response = await mountDataRequest(data);
        await patchUserById(response, id);

        addToast({
          type: 'success',
          title: 'Usuário atualizado',
          description: 'Seu usuário foi atualizado com sucesso!',
        });

        history.push(Paths.User.List);

      } catch (err) {
        addToast({
          type: 'error',
          title: 'Ocorreu um problema',
          description: 'Verifique sua conexão e tente novamente!',
        });
        setError(true);
      } finally {
        setLoading(false);
      }
    },
    [addToast, history],
  );

  const deleteUser = useCallback(
    async (id: string) => {
      try {

        await deleteUserById(id);

        addToast({
          type: 'success',
          title: 'Usuário deletado',
          description: 'Seu usuário foi deletado com sucesso!',
        })

        window.location.reload();

      } catch (err) {
        addToast({
          type: 'error',
          title: 'Ocorreu um problema',
          description: 'Verifique sua conexão e tente novamente!',
        });
      } finally {
        setLoading(false);
      }
    },
    [addToast],
  );



  return {
    findUsers,
    createUser,
    findOneUser,
    updateUser,
    deleteUser,
    usersTable,
    loading,
    error,
    users,
    user
  };
};
