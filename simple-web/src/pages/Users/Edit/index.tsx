import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Container } from './styles';
import { useUser } from '../../../hooks/useUser';
import FormUser from '../../../containers/Form/FormUser'

const EditUser: React.FC = () => {
  const { findOneUser, user } = useUser();
  const { id } = useParams<{ id: string }>();

  useEffect(() => {
    findOneUser(id);
  }, [id, findOneUser]);

  return (
    <Container>
      <FormUser data={user} id={id} />
    </Container>
  );
};

export default EditUser;
