import React, { useEffect } from 'react';
import TableData from '../../components/TableData';
import { useUser } from '../../hooks/useUser';
import { Container } from './styles';

const Users: React.FC = () => {
  const { findUsers, usersTable } = useUser();

  useEffect(() => {
    findUsers();
  }, [findUsers]);

  return (
    <Container>
      <TableData users={usersTable} />
    </Container>
  );
};

export default Users;
