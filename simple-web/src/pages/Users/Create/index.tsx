import React from 'react';
import { Container } from './styles';
import FormUser from '../../../containers/Form/FormUser'

const CreateUser: React.FC = () => {
  return (
    <Container>
      <FormUser />
    </Container>
  );
};

export default CreateUser;
