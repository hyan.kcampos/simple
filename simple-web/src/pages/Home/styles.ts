import styled from 'styled-components';
import img from '../../assets/images/background.png';
import Color from '../../styles/color'

export const Container = styled.div`
  background: ${Color.white};
  margin: 20px 0px 0px 20px;
  border-radius: 4px;
  padding: 20px;

  @media (max-width: 1000px) {
    margin-right: 20px;
  }
`;

export const ContentImage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Image = styled.div`
  background-image: url(${img});
  height: 300px;
  margin-top: 50px;
  background-size: contain;
  background-repeat: no-repeat;
  width: 800px;

  @media (max-width: 540px) {
    height: 150px;
  }
`;

export const ContentInfo = styled.div`
  padding: 20px;
  margin: 0;

  p {
    font-size: 18px;
  }

  b {
    font-size: 18px;
  }
`;
