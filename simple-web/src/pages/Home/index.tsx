import React from 'react';
import { Container, ContentImage, Image, ContentInfo } from './styles'
import { FiFeather } from 'react-icons/fi';
const iconHome = <FiFeather />;

const Home: React.FC = () => {
  return (
    <Container>
      <ContentImage>
        <Image />
      </ContentImage>
      <ContentInfo>
          <p>O <b>Simple</b> veio como seu principal objetivo de <i>"ser uma plataforma simplificada"</i> como seu nome e sua identidade visual {iconHome} já dizem, ele traz
          uma solução simples, objetiva e leve como uma pena. Pensado e contruido com intuito de auxiliar pessoas atravez de tecnologias adaptadas a plataforma.</p>

          <p>Suas principais funcionalidades são: </p>

          <b>Usuários</b>
          <p>Funcionalidade responsável por controlar a listagem, inserção, edição e remoção de todos os usuários inseridos na plataforma. Nesta seção você
            consegue armazenar os principais dados de seus usuários <b>nome, documento, login, senha e endereço</b> para gerenciar como desejar.
          </p>

          <i>Faça bom uso :)</i>
      </ContentInfo>
    </Container>
  );
};

export default Home;
