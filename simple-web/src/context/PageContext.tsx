import React, { createContext, useState, useCallback } from 'react';
import { Page } from '../common/interfaces/page.interface';

const PageContext = createContext<Page>({} as Page);

export const PageProvider: React.FC = ({ children }) => {
  const [page, setPage] = useState('home');

  const handleSetPage = useCallback(data => {
    setPage(data);
  }, []);

  return (
    <PageContext.Provider
      value={{
        page,
        handleSetPage,
      }}
    >
      {children}
    </PageContext.Provider>
  );
};

export default PageContext;
