import axios from 'axios';

import Env from '../../config/env-config';

const baseURL = Env.SIMPLE_API_V1_URL;

const API = axios.create({
  baseURL,
  headers: { 'Content-Type': 'application/json' },
});

export default API;
