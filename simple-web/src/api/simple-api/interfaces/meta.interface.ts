export interface Meta {
  server?: string;
  prev?: string;
  next?: string;
  currentPage?: number;
  offset: number;
  limit: number;
  recordCount: number;
  recordTotal?: number;
}
