export interface UserCreateRequest {
  name: string;
  login: string;
  password?: string;
  document: string;
  cep: string;
  street: string;
  district: string;
  city: string;
  number: string;
}
