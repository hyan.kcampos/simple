import { Meta } from '../../interfaces/meta.interface';

export interface Auth {
  login: string;
  password: string;
}

export interface Address {
  cep: string;
  street: string;
  district: string;
  city: string;
  number: string;
}

export interface User {
  id: string;
  name: string;
  document: string;
  user: Auth;
  address: Address
  created_at: string;
  updated_at: string;
}


export interface UserCreateResponse {
  meta: Meta;
  records: User;
}
