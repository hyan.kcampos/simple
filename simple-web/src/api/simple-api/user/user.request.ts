import { AxiosResponse } from 'axios';

import API from '../base.request';
import { UserManyResponse, UserOneResponse } from './interfaces/user.interface';
import { UserCreateRequest } from './interfaces/userCreateRequest.interface';
import { UserResponse } from '../../../common/interfaces/userResponse.interface';

export const getAllUser = (): Promise<AxiosResponse<UserManyResponse>> => {
  return API.request<UserManyResponse>({
    method: 'GET',
    url: `/user`,
  });
};

export const getUserById = (id: string): Promise<AxiosResponse<UserOneResponse>> => {
  return API.request<UserOneResponse>({
    method: 'GET',
    url: `/user/${id}`,
  });
};

export const postUser = (data: UserCreateRequest): Promise<AxiosResponse<UserManyResponse>> => {
  return API.request<UserManyResponse>({
    method: 'POST',
    url: `/user`,
    data
  });
};

export const patchUserById = (
  data: UserResponse,
  id: string,
): Promise<AxiosResponse<UserOneResponse>> => {
  return API.request<UserOneResponse>({
    method: 'PATCH',
    url: `/user/${id}`,
    data,
  });
};

export const deleteUserById = (id: string): Promise<AxiosResponse> => {
  return API.request({
    method: 'DELETE',
    url: `/user/${id}`,
  });
};
