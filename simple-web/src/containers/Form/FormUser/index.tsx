import React, { useRef, useCallback } from 'react';
import { Form } from '@unform/web';
import { FiFolder, FiLock, FiUser } from 'react-icons/fi';
import { FormHandles } from '@unform/core';
import { Link } from 'react-router-dom';
import { useUser } from '../../../hooks/useUser';
import { useToast } from '../../../hooks/useToast';
import { UserRequest } from '../../../common/interfaces/userRequest.interface';
import { ContentForm, DivisorAddress, FooterForm } from '../styles';
import * as Yup from 'yup';
import Input from '../../../components/Input';
import Button from '../../../components/Button';
import ButtonOutline from '../../../components/Button/Outline';
import getValidationErrors from '../../../utils/getValidationErrors';
import Paths from '../../../constants/paths.constant'
import Color from '../../../styles/color'

interface FormUserProps {
  id?: string;
  data?: UserRequest;
}

const FormUser: React.FC<FormUserProps> = ({ data, id }) => {
  const { createUser, updateUser } = useUser();
  const { addToast } = useToast();

  const formRef = useRef<FormHandles>(null);

  const handleSubmit = useCallback(async (values: Omit<UserRequest, 'id, active, createdAt, updatedAt'>) => {
    try {
      formRef.current?.setErrors({});
      const schema = Yup.object().shape({
        name: Yup.string().required('Nome obrigatório'),
        document: Yup.string()
          .required('CPF obrigatório')
          .matches(
            /^([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})$/, // eslint-disable-line
            'Digite um CPF valido',
          ),
        login: Yup.string()
          .required('Login é obrigatório')
          .min(6, 'No mínimo 6 dígitos'),
        password: id ? Yup.string() : Yup.string().min(6, 'No mínimo 6 dígitos'),
      });
      await schema.validate(values, {
        abortEarly: false,
      });

      if (id && values) {
        await updateUser(id, values);
      } else {
        await createUser(values)
      }

    } catch (error) {
      if (error instanceof Yup.ValidationError) {
        const errors = getValidationErrors(error);
        formRef.current?.setErrors(errors);

        addToast({
          type: 'error',
          title: 'Erro no cadastro',
          description: 'Verifique os campos e tente novamente',
        });
      }
    }
  }, [addToast, updateUser, createUser, id]);

  return (
    <Form ref={formRef} onSubmit={handleSubmit}>
      <ContentForm>
        <h2>{id ? 'Edite seu usuário' : 'Crie seu usuário'}</h2>
        <Input defaultValue={data ? data.name : ''} name="name" icon={FiUser} placeholder="*Nome Completo" />
        <Input defaultValue={data ? data.document : ''} name="document" icon={FiFolder} placeholder="*CPF" />
        <Input defaultValue={data ? data.login : ''} name="login" icon={FiUser} placeholder="*Login" />
        <Input
          name="password"
          icon={FiLock}
          placeholder={data ? "Nova senha" : "*Senha"}
          type="password"
        />
        <DivisorAddress>
          <p>Endereço</p>
          <Input defaultValue={data ? data.cep : ''} name="cep" placeholder="CEP" />
          <Input defaultValue={data ? data.street : ''} name="street" placeholder="Rua" />
          <Input defaultValue={data ? data.district : ''} name="district" placeholder="Bairro" />
          <Input defaultValue={data ? data.city : ''} name="city" placeholder="Cidade" />
          <Input defaultValue={data ? data.number : ''} name="number" placeholder="Numero" />
        </DivisorAddress>
        <FooterForm>
          <Link to={Paths.User.List}>
            <ButtonOutline color={Color.primary.default} border={Color.primary.default}>
              Voltar para listagem
            </ButtonOutline>
          </Link>
          <Button type="submit" color={Color.white} background={Color.primary.default}>
            Salvar alterações
          </Button>
        </FooterForm>
      </ContentForm>
    </Form>
  );
};

export default FormUser;
