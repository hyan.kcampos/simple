import styled from 'styled-components';
import Color from '../../styles/color'

export const ContentForm = styled.div`
  background-color: ${Color.white};
  padding: 20px;
  margin: 20px;
  border-radius: 4px;
`;

export const DivisorAddress = styled.div`
  border: 1px solid ${Color.grey};
  padding: 20px;

  p {
    font-size: 18px;
    color: #1c1c1c;
  }
`;

export const FooterForm = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 20px;
`;
