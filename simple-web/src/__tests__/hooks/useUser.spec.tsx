import MockAdapter from 'axios-mock-adapter';
import { renderHook, act } from '@testing-library/react-hooks';

import API from '../../api/simple-api/base.request';
import { useUser } from '../../hooks/useUser';
import { mockUsersResponse } from '../../__mocks__/user.mocks';
import { mockUserCreate } from '../../__mocks__/userCreate.mocks';
import { mockUserOneResponse } from '../../__mocks__/userResponse.mocks';
import { UserCreateRequest  } from '../../api/simple-api/user/interfaces/userCreateRequest.interface'
import Paths from '../../constants/paths.constant'
import mountDataTable from '../../utils/mountDataTable';
import mountDataResponse from '../../utils/mountDataResponse';

const mock = new MockAdapter(API);
const mockedHistoryPush = jest.fn();
const mockedAddToast = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockedHistoryPush,
  }),
}));

jest.mock("../../hooks/useToast", () => {
  return {
    useToast: () => ({
      addToast: mockedAddToast,
    }),
  };
});


describe('useUser hook', () => {
  afterEach(() => {
    mock.reset();
  });

  describe('findUsers()', () => {
    it('should be able to findUsers with success', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mock.onGet(`/user`).reply(200, mockUsersResponse);

      act(() => {
        result.current.findUsers();
      });

      const valuesMounted = await mountDataTable(mockUsersResponse.records);
      await waitForNextUpdate();

      expect(result.current.usersTable).toEqual(valuesMounted);
      expect(result.current.error).toBe(false);
    });

    it('should be able to set error true when occurs an error on findUsers', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mock.onGet('/user').reply(500);

      act(() => {
        result.current.findUsers();
      });

      await waitForNextUpdate();

      expect(result.current.users).toEqual([]);
      expect(result.current.error).toBe(true);
    });
  });

  describe('createUser(user)', () => {
    it('should be able to create User with success', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mock.onPost('/user').reply(201, mockUserCreate);

      act(() => {
        result.current.createUser(mockUserCreate as UserCreateRequest);
      });

      await waitForNextUpdate();


      expect(mockedHistoryPush).toHaveBeenCalledWith(Paths.User.List);
    });

    it('should be able to set error true when occurs an error on createUser', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      const mockUserCreateNotCPF = {...mockUserCreate, cpf: '44699852145'}

      mock.onPost('/user').reply(409, { data: { field: 'document' }});

      act(() => {
        result.current.createUser(mockUserCreateNotCPF as UserCreateRequest);
      });

      await waitForNextUpdate();

      expect(result.current.users).toEqual([]);
      expect(result.current.error).toBe(true);
    });
  });

  describe('findOneUser(id)', () => {
    it('should be able to find one User with success', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());
      const mockId = '1';

      mock.onGet(`/user/${mockId}`).reply(200, mockUserOneResponse);

      act(() => {
        result.current.findOneUser(mockId);
      });

      const valuesMounted = await mountDataResponse(mockUserOneResponse.records);
      await waitForNextUpdate();

      expect(result.current.user).toEqual(valuesMounted);
    });

    it('should be able to set error true when occurs an error on findOneUser', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());
      const mockId = '1';

      mock.onGet(`/user/${mockId}`).reply(404, { status: 404 });

      act(() => {
        result.current.findOneUser(mockId);
      });

      await waitForNextUpdate();

      expect(result.current.user).toEqual({});
      expect(result.current.error).toBe(true);
    });

    it('should be able to set error true when occurs an error 500 on findOneUser', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());
      const mockId = '1';

      mock.onGet(`/user/${mockId}`).reply(500, { status: 500 });

      act(() => {
        result.current.findOneUser(mockId);
      });

      await waitForNextUpdate();

      expect(result.current.user).toEqual({});
      expect(result.current.error).toBe(true);
    });
  });

  describe('updateUser(id)', () => {
    it('should be able to update a User with success', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());
      const mockId = '1';

      mock.onPatch(`/user/${mockId}`).reply(200, mockUserCreate);

      act(() => {
        result.current.updateUser(mockId, mockUserCreate as UserCreateRequest);
      });

      await waitForNextUpdate();

      expect(mockedHistoryPush).toHaveBeenCalledWith(Paths.User.List);
    });

    it('should be able to handle error when update a User', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());
      const mockId = '27b2be0e-18b0-4f7c-8f98-d79d2ec24bfd';

      mock.onPatch(`/user/${mockId}`).reply(500);

      act(() => {
        result.current.updateUser(mockId, mockUserCreate as UserCreateRequest);
      });

      await waitForNextUpdate();

      expect(result.current.error).toBe(true);
    });
  });

  describe('deleteUser(id)', () => {
    it('should be able to delete a User with success', async () => {
      const { result } = renderHook(() => useUser());
      const mockId = '1';

      mock.onDelete(`/user/${mockId}`).reply(200);

      act(() => {
        result.current.deleteUser(mockId);
      });

      expect(result.current.error).toBe(false);
    });

    it('should be able to handle error when delete a User', async () => {
      const { result } = renderHook(() => useUser());
      const mockId = '1';

      mock.onPatch(`/user/${mockId}`).reply(500);

      act(() => {
        result.current.deleteUser(mockId);
      });

      expect(result.current.error).toBe(false);
    });
  });
});
