import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import CreateUser from '../../../../pages/Users/Create';

describe('CreateUser page', () => {
  it('should be able to render CreateUser page with success', () => {
    const wrapper = mount(
      <Router>
        <CreateUser />
      </Router>,
    );

    expect(wrapper.debug()).toMatchSnapshot();
  });
});
