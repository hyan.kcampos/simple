import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import EditUser from '../../../../pages/Users/Edit';

describe('EditUser page', () => {
  it('should be able to render EditUser page with success', () => {
    const wrapper = mount(
      <Router>
        <EditUser />
      </Router>,
    );

    expect(wrapper.debug()).toMatchSnapshot();
  });
});
