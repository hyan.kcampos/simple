import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import { useUser } from '../../../hooks/useUser';
import { mockUsersAll } from '../../../__mocks__/user.mocks'

import Users from '../../../pages/Users/index';

global.matchMedia = global.matchMedia || function () {
  return {
    addListener: jest.fn(),
    removeListener: jest.fn(),
  };
};

jest.mock('../../../hooks/useUser');

describe('Users page', () => {
  it('should be able to render UsersTable page with success', () => {
    ((useUser as unknown) as jest.Mock).mockReturnValue({
      findUsers: jest.fn(),
      loading: true,
      error: false,
      usersTable: mockUsersAll,
    });

    const wrapper = mount(
      <Router>
        <Users />
      </Router>
    );

    expect(wrapper.debug()).toMatchSnapshot();
  });
});
