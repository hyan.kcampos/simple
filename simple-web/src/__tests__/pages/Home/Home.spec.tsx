import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import Home from '../../../pages/Home';

describe('Home page', () => {
  it('should be able to render Home page with success', () => {
    const wrapper = mount(
      <Router>
        <Home />
      </Router>,
    );

    expect(wrapper.debug()).toMatchSnapshot();
  });
});
