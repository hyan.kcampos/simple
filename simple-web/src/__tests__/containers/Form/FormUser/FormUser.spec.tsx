import React from 'react';
import {
  render,
  fireEvent,
  waitFor,
} from '@testing-library/react';
import { mockUserCreate } from '../../../../__mocks__/userCreate.mocks';
import FormUser from '../../../../containers/Form/FormUser/index';

const mockedHistoryPush = jest.fn();
const mockedError = jest.fn();

jest.mock("react-router-dom", () => {
  return {
    useHistory: () => ({
      push: mockedHistoryPush,
    }),
    Link: ({ children }: { children: React.ReactNode }) => children,
  };
});

jest.mock("../../../../hooks/useToast", () => {
  return {
    useToast: () => ({
      addToast: jest.fn(),
    }),
  };
});

jest.mock("../../../../hooks/useUser", () => {
  return {
    useUser: () => ({
      updateUser: jest.fn(),
      createUser: jest.fn()
    }),
  };
});

describe('FormUser container', () => {
  it('should be able to render FormUser container with success', () => {
    const { asFragment } = render(
        <FormUser data={mockUserCreate} />
    );

    expect(asFragment()).toMatchSnapshot();
  });

  it('should be able to add new user on FormUser container with success', async () => {
    const { getByPlaceholderText, getByText, asFragment } = render(
        <FormUser />
    );

    const nameField = getByPlaceholderText("*Nome Completo");
    const documentField = getByPlaceholderText("*CPF");
    const loginField = getByPlaceholderText("*Login");
    const passwordField = getByPlaceholderText("*Senha");
    const cepField = getByPlaceholderText("CEP");
    const streetField = getByPlaceholderText("Rua");
    const districtField = getByPlaceholderText("Bairro");
    const cityField = getByPlaceholderText("Cidade");
    const numberField = getByPlaceholderText("Numero");
    const buttonElement = getByText("Salvar alterações");

    fireEvent.change(nameField, { target: { value: mockUserCreate.name } });
    fireEvent.change(documentField, { target: { value: mockUserCreate.document } });
    fireEvent.change(loginField, { target: { value: mockUserCreate.login } });
    fireEvent.change(passwordField, { target: { value: mockUserCreate.password } });
    fireEvent.change(cepField, { target: { value: mockUserCreate.cep } });
    fireEvent.change(streetField, { target: { value: mockUserCreate.street } });
    fireEvent.change(districtField, { target: { value: mockUserCreate.district } });
    fireEvent.change(cityField, { target: { value: mockUserCreate.city } });
    fireEvent.change(numberField, { target: { value: mockUserCreate.number } });

    fireEvent.click(buttonElement);

    await waitFor(() => {
      expect(asFragment()).toMatchSnapshot();
    });
  });

  it('should create user with success', async () => {
    const { getByPlaceholderText, getByText, asFragment } = render(
        <FormUser id='1' />
    );

    const nameField = getByPlaceholderText("*Nome Completo");
    const documentField = getByPlaceholderText("*CPF");
    const loginField = getByPlaceholderText("*Login");
    const passwordField = getByPlaceholderText("*Senha");
    const cepField = getByPlaceholderText("CEP");
    const streetField = getByPlaceholderText("Rua");
    const districtField = getByPlaceholderText("Bairro");
    const cityField = getByPlaceholderText("Cidade");
    const numberField = getByPlaceholderText("Numero");
    const buttonElement = getByText("Salvar alterações");

    fireEvent.change(nameField, { target: { value: mockUserCreate.name } });
    fireEvent.change(documentField, { target: { value: mockUserCreate.document } });
    fireEvent.change(loginField, { target: { value: mockUserCreate.login } });
    fireEvent.change(passwordField, { target: { value: mockUserCreate.password } });
    fireEvent.change(cepField, { target: { value: mockUserCreate.cep } });
    fireEvent.change(streetField, { target: { value: mockUserCreate.street } });
    fireEvent.change(districtField, { target: { value: mockUserCreate.district } });
    fireEvent.change(cityField, { target: { value: mockUserCreate.city } });
    fireEvent.change(numberField, { target: { value: mockUserCreate.number } });

    fireEvent.click(buttonElement);

    await waitFor(() => {
      expect(asFragment()).toMatchSnapshot();
    });
  });

  it("should not be able to FormUser with invalid credentials", async () => {
    mockedError.mockImplementation(() => {
      throw new Error();
    });

    const { getByPlaceholderText, getByText } = render(<FormUser />);

    const cpfField = getByPlaceholderText("*CPF");
    const buttonElement = getByText("Salvar alterações");

    fireEvent.change(cpfField, { target: { value: "452" } });

    fireEvent.click(buttonElement);

    await waitFor(() => {
      expect.objectContaining({
        type: "error",
      })
    });
  });
});
