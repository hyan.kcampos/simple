import React from 'react';
import { mount } from 'enzyme';
import DrawerOption from '../../components/Drawer/Option';
import { IoHomeOutline } from 'react-icons/io5';
import { BrowserRouter as Router } from 'react-router-dom';

describe('DrawerOption component', () => {
  it('should be able to render DrawerOption component with success', () => {
    const iconHome = <IoHomeOutline />;

    const wrapper = mount(
      <Router>
        <DrawerOption
          name="Option"
          icon={iconHome}
          page="Home"
          active={true}
        />
      </Router>
    );

    expect(wrapper.debug()).toMatchSnapshot();
  });
});
