import React from 'react';
import ToastContainer from '../../components/ToastContainer';
import { mount } from 'enzyme';

jest.mock('../../hooks/useToast', () => {
  return {
    useToast: () => ({
      removeToast: jest.fn()
    }),
  };
});

describe('ToastContainer component', () => {
  it('should render ToastContainer component with success', () => {
    const message = [{
      id:'1',
      title: 'titulo para teste',
      description: 'descrição para teste'
    }]
    const wrapper = mount(
        <ToastContainer messages={message}/>
    );

    expect(wrapper.debug()).toMatchSnapshot();
  });

  it('should be remove toast of ToastContainer component with success', () => {
    const message = [{
      id:'1',
      title: 'titulo para teste',
      description: 'descrição para teste'
    }]
    const wrapper = mount(
        <ToastContainer messages={message}/>
    );

    const button = wrapper.find('button').at(0);
    button.simulate('click');


    expect(wrapper.debug()).toMatchSnapshot();
  });
});
