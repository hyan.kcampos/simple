import React from 'react';
import TableData from '../../components/TableData';
import { mount } from 'enzyme';
import { mockUsersAll } from '../../__mocks__/user.mocks'
import { BrowserRouter as Router } from 'react-router-dom';
import { fireEvent, render } from '@testing-library/react';

global.matchMedia = global.matchMedia || function () {
  return {
    addListener: jest.fn(),
    removeListener: jest.fn(),
  };
};

describe('TableData component', () => {
  it('should render TableData component with success', () => {
    const wrapper = mount(
      <Router>
        <TableData users={mockUsersAll} />
      </Router>
    );

    expect(wrapper.debug()).toMatchSnapshot();
  });

  it('should render simulate click button Edit of TableData component with success', () => {
    const {getByTestId} = render(
      <Router>
        <TableData users={mockUsersAll} />
      </Router>
    )

    const button = fireEvent.click(getByTestId('button-edit'))

    expect(button).toBe(true);
  });

  it('should render simulate click button Delete of TableData component with success', async () => {
    const {getByTestId} = render(
      <Router>
        <TableData users={mockUsersAll} />
      </Router>
    )

    const button = fireEvent.click(getByTestId('button-delete'))

    expect(button).toBe(true);
  });
});
