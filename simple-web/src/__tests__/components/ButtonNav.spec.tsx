import React from 'react';

import {
  render,
  fireEvent,
  waitFor,
} from '@testing-library/react';

import ButtonNav from '../../components/Button/Nav';
import PageContext, { PageProvider } from '../../context/PageContext';

const mockedHistoryPush = jest.fn();

jest.mock("react-router-dom", () => {
  return {
    useHistory: () => ({
      push: mockedHistoryPush,
    }),
    Link: ({ children }: { children: React.ReactNode }) => children,
  };
});


describe('ButtonNav component', () => {
  beforeEach(() => {
    mockedHistoryPush.mockClear();
  });

  it('should be able to render ButtonNav component with success', async () => {
    const props = {
      handleSetPage: () => jest.fn(),
      page: 'Home'
    };

    const { getByTestId } = render(
      <PageProvider>
        <PageContext.Provider value={props}>
          <ButtonNav active={true} name="Teste" />);
        </PageContext.Provider>
      </PageProvider>
    );

    const navButton = getByTestId('button-nav');
    fireEvent.click(navButton);

    await waitFor(() => {
      expect(mockedHistoryPush).toHaveBeenCalledWith("/users");
    });
  });
});
