import React from 'react';
import Tooltip from '../../components/Tooltip';
import { mount } from 'enzyme';

describe('Tooltip component', () => {
  it('should render Tooltip component with success', () => {
    const wrapper = mount(
        <Tooltip title="teste" className="name-teste" />
    );

    expect(wrapper.debug()).toMatchSnapshot();
  });

  it('should render Tooltip component not passed property className with success', () => {
    const wrapper = mount(
        <Tooltip title="teste" />
    );

    expect(wrapper.debug()).toMatchSnapshot();
  });
});
