import React from 'react';
import { mount } from 'enzyme';

import ButtonOutline from '../../components/Button/Outline';
import Color from '../../styles/color'

describe('ButtonOutline component', () => {
  it('should be able to render ButtonOutline component with success', () => {
    const wrapper = mount(<ButtonOutline color={Color.white} border={Color.primary.default} />);

    expect(wrapper.debug()).toMatchSnapshot();
  });
});
