import React from 'react';
import Input from '../../components/Input';

import { fireEvent, render, waitFor } from '@testing-library/react';

jest.mock('@unform/core', () => {
  return {
    useField() {
      return {
        fieldName: 'email',
        defaultValue: '',
        error: '',
        registerField: jest.fn(),
      }
    }
  }
})

describe('Input component', () => {
  it('should be able to render Input component with success', () => {
    const { getByPlaceholderText } = render(
      <Input name='email' placeholder='E-mail'/>
    )
    expect(getByPlaceholderText('E-mail')).toBeTruthy();
  });

  it('Should render highlight on input focus', async () => {
    const { getByPlaceholderText, getByTestId } = render(
      <Input name='email' placeholder='E-mail' />
    )

    const inputElement = getByPlaceholderText('E-mail');
    const containerElement = getByTestId('input-container');

    fireEvent.focus(inputElement);

    await waitFor(() => {
      expect(containerElement).toHaveStyle('border-color: #3F2CB1')
      expect(containerElement).toHaveStyle('color: #3F2CB1')
    })

    fireEvent.blur(inputElement);

    await waitFor(() => {
      expect(containerElement).not.toHaveStyle('border-color: #3F2CB1')
      expect(containerElement).not.toHaveStyle('color: #3F2CB1')
    })
  })
});
