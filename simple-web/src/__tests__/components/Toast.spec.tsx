import React from 'react';
import Toast from '../../components/ToastContainer/Toast';
import { mount } from 'enzyme';
import { useToast } from '../../hooks/useToast'

jest.mock('../../hooks/useToast');


describe('Toast component', () => {
  it('should render Toast component with success', () => {
    ((useToast as unknown) as jest.Mock).mockReturnValue({
      removeToast: jest.fn(),
    });
    const message = {
      id:'1',
      title: 'titulo para teste',
      description: 'descrição para teste'
    }
    const wrapper = mount(
        <Toast style={{hasDescription: 'teste'}} message={message} />
    );

    expect(wrapper.debug()).toMatchSnapshot();
  });
});
