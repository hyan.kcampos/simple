import React from 'react';
import { mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import Drawer from '../../components/Drawer';

global.matchMedia = global.matchMedia || function () {
  return {
    addListener: jest.fn(),
    removeListener: jest.fn(),
  };
};

describe('Drawer component', () => {
  it('should be able to render Drawer component with success', () => {
    const wrapper = mount(<Router><Drawer /></Router>);

    expect(wrapper.debug()).toMatchSnapshot();
  });
});
