import React from 'react';
import Strip from '../../components/Strip';
import { mount } from 'enzyme';

describe('Strip component', () => {
  it('should render Strip component with success', () => {
    const wrapper = mount(<Strip />);

    expect(wrapper.debug()).toMatchSnapshot();
  });
});
