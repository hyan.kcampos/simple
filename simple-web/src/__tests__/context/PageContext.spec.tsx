import React from 'react';
import PageContext, { PageProvider } from '../../context/PageContext';

import ButtonNav from '../../components/Button/Nav'
import {
  render, act
} from '@testing-library/react';


describe('Page Context', () => {
  it('renders Page Context Context correctly', async () => {
    const props = {
      handleSetPage: () => jest.fn(),
      page: 'Home',
    };
    const tree = render(
      <PageProvider>
        <PageContext.Provider value={props}>
          <ButtonNav active={true} name='button'/>
        </PageContext.Provider>
      </PageProvider>
    );

    await act(async () => {
      expect(tree).toMatchSnapshot();
    });
  });
});
