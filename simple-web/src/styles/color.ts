const Color = {
  primary: {
    default: '#3F2CB1',
    light: '#cecfff'
  },
  primary110: '#0079E6',
  grey40: '#424242',
  grey30: '#a7a7a7',
  grey20: '#e5e5e5',
  grey10: '#F3F3F3',
  grey: '#c5c5c5',
  white: '#FFFFFF',
  white_opacity: '#FFF9',
  info: {
    default: '#369FF4',
    light: '#E8F4FD',
  },
  success: {
    default: '#6cc070',
    light: '#CCF7BB',
    dark: '#3A9E12',
  },
  error: {
    default: '#c53030',
    light: '#ffd2d2',
  },
  alert: {
    default: '#FFDC83',
    dark: '#FB9600',
  },
  text: '#555555',
  background: '#EDEDED',
};

export default Color;
