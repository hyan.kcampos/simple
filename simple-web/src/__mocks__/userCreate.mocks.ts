const mockUserCreate = {
    name: 'Hyan',
    document: '05899852145',
    login: 'hy_campos_mock',
    password: '12345678',
    cep: '14402548',
    street: 'Ana lucia da silva freitas',
    district: 'Centro',
    city: 'Franca',
    number: '5478'
}

export {
  mockUserCreate,
};
