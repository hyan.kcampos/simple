const mockUserOneResponse = {
  records: {
    id: '1',
    name: 'Hyan',
    document: '44699852145',
    user: {
      login: 'hy_campos',
      password: '12345678',
    },
    address: {
      cep: '14402548',
      street: 'Ana lucia da silva freitas',
      district: 'Centro',
      city: 'Franca',
      number: '5478'
    },
    created_at: '2022-01-16T01:10:09.000Z',
    updated_at: '2022-01-16T01:10:09.000Z'
  }
}

export {
  mockUserOneResponse,
};
