<div align="center">
<br>
  <img src="simple-web/src/assets/logos/logo.png" alt="papitos" width="120">
<br>
</div>

<p>
 <a href="#-sobre-o-projeto">Sobre</a>
</p>
<p>
 <a href="#-funcionalidades">Funcionalidades</a>
</p>
<p>
 <a href="#-layout">Layout</a>
</p>
<p>
 <a href="#-como-executar-o-projeto">Como executar</a>
</p>
<p>
 <a href="#-tecnologias">Tecnologias</a>
</p>
<p>
 <a href="#-contribuidores">Contribuidores</a>
</p>
<p>
 <a href="#-autor">Autor</a>
</p>


## 💻 Sobre o projeto

Simple 

O <b>Simple</b> veio como seu principal objetivo de <i>"ser uma plataforma simplificada"</i> como seu nome e sua identidade visual já dizem, ele traz uma solução simples, objetiva e leve como uma pena. Pensado e contruido com intuito de auxiliar pessoas atravez de tecnologias adaptadas a plataforma.</p>

---

## ⚙️ Funcionalidades

- [x] Home:
  - [x] Pagina "mostruario" com o principal objetivo da aplicação

- [x] Usuários:
  - [x] Cadastrar novos usuários
  - [x] Editar usuários
  - [x] Deletar usuários
  - [x] Visualizar listagem usuários

---

## 🎨 Layout

### Web

<p align="center" style="display: flex; align-items: flex-start; justify-content: center;">
  <img alt="home" title="#home" src="./simple-web/src/assets/images/screenshot-web-home.png">
  <img alt="users-list" title="#users-list" src="./simple-web/src/assets/images/screenshot-web-users-list.png">
  <img alt="users" title="#users" src="./simple-web/src/assets/images/screenshot-web-users.png">
</p>

### Mobile


<p align="center" style="display: flex; align-items: flex-start; justify-content: center;">
  <img alt="home" title="#home" src="./simple-web/src/assets/images/screenshot-mobile-home.png" width="250px">
  <img alt="users-list" title="#users-list" src="./simple-web/src/assets/images/screenshot-mobile-users-list.png" width="250px">
  <img alt="users" title="#users" src="./simple-web/src/assets/images/screenshot-mobile-users.png" width="250px">
</p>

---

## 🚀 Como executar o projeto

Este projeto é divido em três partes:
1. Backend (pasta simple-api) 
2. Frontend (pasta simple-web)
3. Banco de dados (criado automático pelo docker)

💡 Este é um projeto construido em cima do da ferramenta Docker, a instalação e configuração de execução do
projeto a seguir será em cima desta ferramenta.

### Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Docker Compose](https://docs.docker.com/compose/install/). Para ajudar, recomendo um bom editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/)

#### 🎲 Adquira o repositorio do projeto (simple)

```bash

# Clone este repositório
$ git clone https://gitlab.com/hyan.kcampos/simple.git


```

#### 🎲 Configurando o Backend (simple-api)

```bash

# Abra o projeto em um editor ou cmd na pasta simple-api
$ cd simple-api

# Crie um arquivo .env na raiz da pasta simple-api e coloque as seguinte instrução
$ API_PORT=8080

```

#### 🎲 Configurando o Frontend (simple-web)

```bash

# Abra o projeto em um editor ou cmd na pasta simple-web
$ cd simple-web

# Crie um arquivo .env na raiz da pasta simple-web e coloque as seguinte instrução
$ REACT_APP_SIMPLE_API_V1_URL=http://localhost:8080

```

#### 🎲 Executando a aplicação (simple)

```bash

# Volte para a pasta simple
$ cd ..

# Rode seguinte comando no terminal
$ docker-compose up --build

Este comando pode solicitar sua senha, caso não solicitar você pode iniciar com o sudo na frente
sudo docker-compose up --build 

```
<p>✅ Pronto, seu app estará rodando no host: (http://localhost:3001/)</p>

---

## 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

#### **Frontend**  ([React](https://reactjs.org/)  +  [TypeScript](https://www.typescriptlang.org/))

-   **[React Router Dom](https://github.com/ReactTraining/react-router/tree/master/packages/react-router-dom)**
-   **[React Icons](https://react-icons.github.io/react-icons/)**
-   **[Axios](https://github.com/axios/axios)**
-   **[Jest](https://jestjs.io/)**
-   **[Eslint](https://eslint.org/)**
-   **[Prettier](https://prettier.io/)**
-   **[Docker](https://www.docker.com/)**

> Veja o arquivo  [package.json](https://gitlab.com/hyan.kcampos/simple/-/blob/main/simple-web/package.json)

#### [](https://github.com/tgmarinho/Ecoleta#server-nodejs--typescript)**Backend**  ([NestJS](https://nestjs.com/)  +  [TypeScript](https://www.typescriptlang.org/))

-   **[Express](https://expressjs.com/)**
-   **[CORS](https://expressjs.com/en/resources/middleware/cors.html)**
-   **[ts-node](https://github.com/TypeStrong/ts-node)**
-   **[Swagger](https://swagger.io/)**
-   **[Axios](https://github.com/axios/axios)**
-   **[Jest](https://jestjs.io/)**
-   **[Eslint](https://eslint.org/)**
-   **[Prettier](https://prettier.io/)**
-   **[Docker](https://www.docker.com/)**

> Veja o arquivo  [package.json](https://gitlab.com/hyan.kcampos/simple/-/blob/main/simple-api/package.json)

#### [](https://github.com/tgmarinho/Ecoleta#mobile-react-native--typescript)**Banco de dados**  ([Mysql](https://www.mysql.com/))

-   **[Docker](https://www.docker.com/)**

#### [](https://github.com/tgmarinho/Ecoleta#utilit%C3%A1rios)**Utilitários**

-   Modelagem Banco de Dados:  **[Drive](https://drive.google.com/file/d/1i-epXt3F4FrWEnW6cmgpHS56lFfUJpp1/view?usp=sharing)**
-   Teste Frontend:  **91.02%** [Drive](https://drive.google.com/file/d/1JPmocK5-hELGdzMDcicUDfhGVv51kpru/view?usp=sharing)
-   Teste Backend:  **97.48%** [Drive](https://drive.google.com/file/d/1zUClqK9D9oZFuknzi_c_5BJrBHsRknr4/view?usp=sharing)
-   Editor:  **[Visual Studio Code](https://code.visualstudio.com/)**
-   Ícones:  **[Feather Icons](https://feathericons.com/)**,  **[Font Awesome](https://fontawesome.com/)**
-   Fontes:  **[Ubuntu](https://fonts.google.com/specimen/Ubuntu)**,  **[Roboto](https://fonts.google.com/specimen/Roboto)**


---

## 👨‍💻 Contribuidores
<table>
  <tr>
    <td align="center"><a href="https://github.com/hyankelwin"><img style="border-radius: 50%;" src="https://gitlab.com/uploads/-/system/user/avatar/9851036/avatar.png" width="100px;" alt=""/><br /><sub><b>Hyan Kelwin</b></sub></a><br /><a href="https://github.com/hyankelwin" >👨‍🚀</a></td>
  </tr>
</table>

## 💪 Como contribuir para o projeto

1. Faça um **fork** do projeto.
2. Crie uma nova branch com as suas alterações: `git checkout -b my-feature`
3. Salve as alterações e crie uma mensagem de commit contando o que você fez: `git commit -m "feature: My new feature"`
4. Envie as suas alterações: `git push origin my-feature`
> Caso tenha alguma dúvida, confira este [guia de como contribuir no GitHub](https://www.linkedin.com/pulse/como-contribuir-em-um-projeto-open-source-github-f%C3%A1bio-amaral/?originalSubdomain=pt)

---

## 🦸 Autor
 
Desenvolvedor apaixonado por programação e tecnologias.

<a href="https://github.com/hyankelwin">
 <img style="border-radius: 50%;" src="https://gitlab.com/uploads/-/system/user/avatar/9851036/avatar.png" width="100px;" alt=""/>
 <br />
 <sub><b>Hyan Kelwin</b></sub></a> <a href="https://github.com/hyankelwin" title="GitHub Hyan">🚀</a>
 <br />

---
