**Descrição**


**Task no Jira**


**Como reproduzir o comportamento**


**Checklist**

- [ ] Testes unitários cobrem o bug corrigido
- [ ] Realizei o teste na local
- [ ] Tem dependência externa para publicação (api, projeto ou time)
- [ ] Commits seguem [conventional-commits](https://www.conventionalcommits.org/en/v1.0.0/)
- [ ] Changelog em `### Fixed`


**Prints (Antes e depois, mobile e desk)**


**Observações**


---
~bug
