import { User } from '../api/user/user.entity';

export class ClassMock {
  public static CreateNewUser(): User {
    return {
      id: 1,
      name: 'Hyan',
      document: '44699588745',
      login: 'hy_campos',
      password: '12345678',
      cep: '14412410',
      street: 'Ana lucia da silva cintra',
      district: 'Centro',
      city: 'Franca',
      number: '5470',
    } as User;
  }

  public static CreateNewUsers(): User[] {
    return [
      {
        id: 1,
        name: 'Hyan',
        document: '44699588745',
        login: 'hy_campos',
        password: '12345678',
        cep: '14412410',
        street: 'Ana lucia da silva cintra',
        district: 'Centro',
        city: 'Franca',
        number: '5470',
      },
      {
        id: 2,
        name: 'Lucas',
        document: '02789588745',
        login: 'lu_silva',
        password: '54321',
        cep: '14412590',
        street: 'Av brasil',
        district: 'Centro',
        city: 'Franca',
        number: '960',
      },
    ] as User[];
  }
}
