import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './api/user/user.module';

const environment = process.env.NODE_ENV || 'development';

@Module({
  imports: [
    UserModule,
    TypeOrmModule.forRoot(),
    ConfigModule.forRoot({
      //disable .env file load when in production environment
      ignoreEnvFile: process.env.NODE_ENV === 'production',
      envFilePath: `.env-${environment}`,
      isGlobal: true,
    }),
  ],
})
export class AppModule {}
