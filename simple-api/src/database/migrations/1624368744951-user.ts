import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class user1624368744951 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'user',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
            isNullable: false,
          },
          {
            name: 'name',
            type: 'varchar',
            length: '250',
            isNullable: false,
          },
          {
            name: 'document',
            type: 'varchar',
            length: '45',
            isNullable: false,
          },
          {
            name: 'login',
            type: 'varchar',
            length: '45',
            isNullable: false,
          },
          {
            name: 'password',
            type: 'varchar',
            length: '150',
            isNullable: false,
          },
          {
            name: 'cep',
            type: 'varchar',
            length: '45',
            isNullable: true,
          },
          {
            name: 'street',
            type: 'varchar',
            length: '150',
            isNullable: true,
          },
          {
            name: 'district',
            type: 'varchar',
            length: '150',
            isNullable: true,
          },
          {
            name: 'city',
            type: 'varchar',
            length: '150',
            isNullable: true,
          },
          {
            name: 'number',
            type: 'varchar',
            length: '45',
            isNullable: true,
          },

          { name: 'created_at', type: 'timestamp', default: 'now()' },
          { name: 'updated_at', type: 'timestamp', default: 'now()' },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.clearSqlMemory();
    await queryRunner.dropTable('user');
  }
}
