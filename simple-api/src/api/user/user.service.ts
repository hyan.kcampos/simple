import { Injectable } from '@nestjs/common';
import { User } from './user.entity';
import { UserRepository } from './user.repository';
import { HttpException, HttpStatus } from '@nestjs/common';
import { UserPatchDTO } from './dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  async saveOrUpdate(user: User): Promise<User> {
    await this.isDocumentExist(user);
    await this.isLoginExist(user);
    const hash = await this.generateHashPassword(user.password);
    user.password = hash;
    return this.userRepository.saveOrUpdate(user);
  }

  async findAll(): Promise<User[]> {
    return await this.userRepository.find();
  }

  async findById(id: number): Promise<User> {
    const response = await this.userRepository.findById(id);

    if (!response) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'this ID not exist',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    return response;
  }

  async updateUser(userId: number, body: UserPatchDTO): Promise<User> {
    const isUser = await this.userRepository.findById(userId);

    if (!isUser) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'this user not exist',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    if (body.user.password) {
      const isPasswordChanged = await this.compareHashPassword(
        body.user.password,
        isUser.password,
      );

      if (isPasswordChanged) {
        const newHash = await this.generateHashPassword(body.user.password);
        isUser.password = newHash;
      }
    }

    const newUser = {
      id: isUser.id,
      name: body.name,
      document: body.document,
      login: body.user.login,
      password: isUser.password,
      cep: body.address.cep,
      street: body.address.street,
      district: body.address.district,
      city: body.address.city,
      number: body.address.number,
    };

    return this.userRepository.saveOrUpdate(newUser);
  }

  async deleteUser(id: number): Promise<User> {
    const response = await this.userRepository.findById(id);

    if (!response) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'this ID not exist',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    await this.userRepository.delete(id);
    return response;
  }

  async isDocumentExist(user: User): Promise<User> {
    const existDocument = await this.userRepository.find({
      where: { document: user.document },
    });

    if (existDocument.length) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          data: { field: 'document' },
          error: 'there is already a user with this document',
        },
        HttpStatus.CONFLICT,
      );
    }

    return user;
  }

  async isLoginExist(user: User): Promise<User> {
    const existLogin = await this.userRepository.find({
      where: { login: user.login },
    });

    if (existLogin.length) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          data: { field: 'login' },
          error: 'there is already a user with this login',
        },
        HttpStatus.CONFLICT,
      );
    }

    return user;
  }

  async compareHashPassword(password: string, hash: string): Promise<boolean> {
    const isMatch = await bcrypt.compare(password, hash);
    return !isMatch;
  }

  async generateHashPassword(password: string): Promise<string> {
    const saltOrRounds = 10;
    const hash = await bcrypt.hash(password, saltOrRounds);

    return hash;
  }
}
