import { ConfigService } from '@nestjs/config';
import { UserResponseDTO, UserRequestDTO, UserPatchDTO } from './dto';
import { UserController } from './user.controller';
import { UserRepository } from './user.repository';
import { UserService } from './user.service';

describe('Testing User controller', () => {
  let userController: UserController;
  let userService: UserService;
  let configService: ConfigService;
  let userRepository: UserRepository;

  beforeAll(async () => {
    userRepository = new UserRepository();
    configService = new ConfigService();
    userService = new UserService(userRepository);
    userController = new UserController(userService);
  });

  const id = 1;
  const user = {
    id: 1,
    name: 'Hyan',
    document: '44699482254',
    login: 'hy_campos',
    password: '123456',
    cep: '4455542',
    street: 'Deolinda da silva',
    district: 'centro',
    city: 'Franca',
    number: '5477',
  };

  it('Testing method save', async () => {
    const userDTO = {
      name: 'Hyan',
      document: '44699474585',
      login: 'hy_campos',
      password: '123456',
      cep: '4455542',
      street: 'Deolinda da silva',
      district: 'centro',
      city: 'Franca',
      number: '5477',
    } as UserRequestDTO;
    userService.saveOrUpdate = jest.fn().mockImplementation(() => user);
    expect(await userController.save(userDTO)).toStrictEqual(
      UserResponseDTO.fromOneEntity(user),
    );
  });

  it('Testing method patch', async () => {
    const userPatchDTO = {
      name: 'Hyan',
      document: '44699474585',
      user: {
        login: 'hy_campos',
        password: '123456',
      },
      address: {
        cep: '4455542',
        street: 'Deolinda da silva',
        district: 'centro',
        city: 'Franca',
        number: '5477',
      },
    } as UserPatchDTO;

    userService.updateUser = jest.fn().mockImplementation(() => user);

    expect(await userController.updateUserById(1, userPatchDTO)).toStrictEqual(
      UserResponseDTO.fromOneEntity(user),
    );
  });

  it('Testing method deleteUserById', async () => {
    userService.deleteUser = jest.fn().mockImplementation(() => user);
    expect(await userController.deleteUserById(id)).toStrictEqual(
      UserResponseDTO.fromOneEntity(user),
    );
  });
});
