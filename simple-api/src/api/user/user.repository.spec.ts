import { ClassMock } from '../../mocks/class.mock';
import { UserRepository } from './user.repository';

describe('Testing user repository', () => {
  let userRepository: UserRepository;
  const mockedUser = ClassMock.CreateNewUser();

  const createQueryBuilder: any = {
    select: () => createQueryBuilder,
    where: () => createQueryBuilder,
    getOne: () => mockedUser,
  };

  beforeAll(() => {
    userRepository = new UserRepository();
  });

  it('Testing method findById', async () => {
    const id = mockedUser.id;
    const spiedQueryBuilder = jest
      .spyOn(userRepository, 'createQueryBuilder')
      .mockImplementation(() => createQueryBuilder);
    expect(await userRepository.findById(id)).toMatchObject(mockedUser);
    expect(spiedQueryBuilder).toBeCalled();
  });

  it('Testing method saveOrUpdate', async () => {
    const spiedSave = jest.spyOn(userRepository, 'save');
    spiedSave.mockResolvedValueOnce(mockedUser);
    expect(await userRepository.saveOrUpdate(mockedUser));
    expect(spiedSave).toBeCalled();
  });
});
