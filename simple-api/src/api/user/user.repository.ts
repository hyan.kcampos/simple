import { EntityRepository, Repository } from 'typeorm';
import { User } from './user.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async findById(id: any): Promise<User> {
    return this.createQueryBuilder('user')
      .where('user.id =:id', { id })
      .getOne();
  }

  async saveOrUpdate(user: User): Promise<User> {
    const response = await this.save(user);
    return this.findById(response.id);
  }
}
