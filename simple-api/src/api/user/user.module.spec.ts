import { Test } from '@nestjs/testing';
import { UserModule } from './user.module';

describe('Testing import User module', () => {
  it('Should import User module succesfully', async () => {
    try {
      const module = await Test.createTestingModule({
        imports: [UserModule],
      }).compile();
      expect(module).not.toBeNull();
    } catch (err) {
      expect(err).not.toBeNull();
    }
  });
});
