import { UserPatchDTO } from './userPatch.dto';
import { HttpException } from '@nestjs/common';

describe('Testing UserPatch DTO', () => {
  describe('Testing loading UserPatch DTO', () => {
    it('Should create UserPatch DTO succesfully', async () => {
      const userDTO: UserPatchDTO = new UserPatchDTO();
      expect(userDTO).not.toBeNull();
    });
  });

  describe('Testing static method isValid()', () => {
    it('Validates if the object is correct', async () => {
      const userPatchDTO: UserPatchDTO = {
        name: 'Hyan',
        document: '44699588745',
        user: {
          login: 'hy_campos',
          password: '12345678',
        },
        address: {
          cep: '14412410',
          street: 'Ana lucia da silva cintra',
          district: 'Centro',
          city: 'Franca',
          number: '5470',
        },
      };
      expect(UserPatchDTO.isValid(userPatchDTO)).toBe(userPatchDTO);
    });

    it('Should give error when document is null', async () => {
      try {
        const userPatchDTO: UserPatchDTO = {
          name: 'Hyan',
          document: null,
          user: {
            login: 'hy_campos',
            password: '12345678',
          },
          address: {
            cep: '14412410',
            street: 'Ana lucia da silva cintra',
            district: 'Centro',
            city: 'Franca',
            number: '5470',
          },
        };
        UserPatchDTO.isValid(userPatchDTO);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });

    it('Should give error when name is null', async () => {
      try {
        const userPatchDTO: UserPatchDTO = {
          name: null,
          document: '44699588745',
          user: {
            login: 'hy_campos',
            password: '12345678',
          },
          address: {
            cep: '14412410',
            street: 'Ana lucia da silva cintra',
            district: 'Centro',
            city: 'Franca',
            number: '5470',
          },
        };
        UserPatchDTO.isValid(userPatchDTO);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });

    it('Should give error when login is null', async () => {
      try {
        const userPatchDTO: UserPatchDTO = {
          name: 'Hyan',
          document: '44699588745',
          user: {
            login: null,
            password: '12345678',
          },
          address: {
            cep: '14412410',
            street: 'Ana lucia da silva cintra',
            district: 'Centro',
            city: 'Franca',
            number: '5470',
          },
        };
        UserPatchDTO.isValid(userPatchDTO);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });
  });
});
