import { ApiProperty } from '@nestjs/swagger';
import { User } from '../user.entity';
import { isNotEmpty } from 'class-validator';
import { HttpException, HttpStatus } from '@nestjs/common';

export class UserRequestDTO {
  @ApiProperty({ example: 'id' })
  id: number;

  @ApiProperty({ example: 'Hyan' })
  name: string;

  @ApiProperty({ example: '44699474825' })
  document: string;

  @ApiProperty({ example: 'hy_campos' })
  login: string;

  @ApiProperty({ example: '123456' })
  password: string;

  @ApiProperty({ example: '14402741' })
  cep: string;

  @ApiProperty({ example: 'Ana Lucia Alves da Silva' })
  street: string;

  @ApiProperty({ example: 'Centro' })
  district: string;

  @ApiProperty({ example: 'Franca' })
  city: string;

  @ApiProperty({ example: '5420' })
  number: string;

  public static isValid(userDTO: UserRequestDTO): UserRequestDTO {
    const { name, document, login, password } = userDTO;

    const isDocumentEmpty = !isNotEmpty(document);

    if (isDocumentEmpty) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'CPF is required',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const isNameEmpty = !isNotEmpty(name);

    if (isNameEmpty) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'name is required',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const isLoginEmpty = !isNotEmpty(login);

    if (isLoginEmpty) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'login is required',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const isPasswordEmpty = !isNotEmpty(password);

    if (isPasswordEmpty) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'password is required',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    return userDTO;
  }

  public static toEntity(userRequestDTO: UserRequestDTO): User {
    return new User({
      name: userRequestDTO.name,
      document: userRequestDTO.document,
      login: userRequestDTO.login,
      password: userRequestDTO.password,
      cep: userRequestDTO.cep,
      street: userRequestDTO.street,
      district: userRequestDTO.district,
      city: userRequestDTO.city,
      number: userRequestDTO.number,
    });
  }
}
