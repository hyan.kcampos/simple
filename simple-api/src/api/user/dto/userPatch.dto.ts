import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { UserCommonDTO, AddressCommonDTO } from '../../../common/dto';
import { isNotEmpty } from 'class-validator';
import { HttpException, HttpStatus } from '@nestjs/common';

export class UserPatchDTO {
  @ApiProperty({ example: 'Hyan' })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ example: '44688455472' })
  @IsString()
  @IsNotEmpty()
  document: string;

  @ApiProperty({ type: UserCommonDTO })
  @IsString()
  @IsNotEmpty()
  user: UserCommonDTO;

  @ApiProperty({ type: AddressCommonDTO })
  @IsString()
  address: AddressCommonDTO;

  static isValid(params: UserPatchDTO): UserPatchDTO {
    const { document, name, user } = params;

    const isDocumentEmpty = !isNotEmpty(document);

    if (isDocumentEmpty) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'CPF is required',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const isNameEmpty = !isNotEmpty(name);

    if (isNameEmpty) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'name is required',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const isLoginEmpty = !isNotEmpty(user.login);

    if (isLoginEmpty) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'login is required',
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    return params;
  }
}
