import { ApiProperty } from '@nestjs/swagger';
import { User } from '../user.entity';
import { IsNotEmpty, IsString } from 'class-validator';
import { UserCommonDTO, AddressCommonDTO } from '../../../common/dto';

export class UserResponseDTO {
  @ApiProperty({ example: 1 })
  id?: number;

  @ApiProperty({ example: 'Hyan' })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ example: '44688452452' })
  @IsString()
  @IsNotEmpty()
  document: string;

  @ApiProperty({ type: UserCommonDTO })
  @IsString()
  @IsNotEmpty()
  user: UserCommonDTO;

  @ApiProperty({ type: AddressCommonDTO })
  @IsString()
  address: AddressCommonDTO;

  @ApiProperty({ example: new Date() })
  created_at?: Date;

  @ApiProperty({ example: new Date() })
  updated_at?: Date;

  public static fromOneEntity(user: User): UserResponseDTO {
    return this.mountResponse(user) as UserResponseDTO;
  }

  public static fromManyEntity(users: User[]): UserResponseDTO[] {
    return users.map((user) => {
      return this.mountResponse(user);
    });
  }

  public static mountResponse(user: User) {
    return {
      id: user.id,
      name: user.name,
      document: user.document,
      user: {
        login: user.login,
        password: user.password,
      },
      address: {
        cep: user.cep,
        street: user.street,
        district: user.district,
        city: user.city,
        number: user.number,
      },
      created_at: user.created_at,
      updated_at: user.updated_at,
    };
  }
}
