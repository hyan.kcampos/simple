import { UserResponseDTO } from './userResponse.dto';
import { User } from '../user.entity';

describe('Testing UserResponse DTO', () => {
  describe('Testing loading UserResponse DTO', () => {
    it('Should create UserResponse DTO succesfully', async () => {
      const userDTO: UserResponseDTO = new UserResponseDTO();
      expect(userDTO).not.toBeNull();
    });
  });

  describe('Testing static method fromOneEntity()', () => {
    it('Validates if the object is correct', async () => {
      const userDTO: User = {
        id: 1,
        name: 'Hyan',
        document: '44699588745',
        login: 'hy_campos',
        password: '12345678',
        cep: '14412410',
        street: 'Ana lucia da silva cintra',
        district: 'Centro',
        city: 'Franca',
        number: '5470',
      };

      const mountResponse = {
        id: 1,
        name: 'Hyan',
        document: '44699588745',
        user: {
          login: 'hy_campos',
          password: '12345678',
        },
        address: {
          cep: '14412410',
          street: 'Ana lucia da silva cintra',
          district: 'Centro',
          city: 'Franca',
          number: '5470',
        },
        created_at: jest
          .useFakeTimers('modern')
          .setSystemTime(new Date(2020, 9, 1, 7)),
        updated_at: jest
          .useFakeTimers('modern')
          .setSystemTime(new Date(2020, 9, 1, 7)),
      };

      expect(UserResponseDTO.fromOneEntity(userDTO)).toStrictEqual(
        mountResponse,
      );
    });
  });

  describe('Testing static method fromManyEntity()', () => {
    it('Validates if the object is correct', async () => {
      const usersDTO: User[] = [
        {
          id: 1,
          name: 'Hyan',
          document: '44699588745',
          login: 'hy_campos',
          password: '12345678',
          cep: '14412410',
          street: 'Ana lucia da silva cintra',
          district: 'Centro',
          city: 'Franca',
          number: '5470',
        },
        {
          id: 2,
          name: 'Leonardo',
          document: '03699588745',
          login: 'le_silva',
          password: '12345678',
          cep: '14412785',
          street: 'Av brasil',
          district: 'Jardim centenario',
          city: 'Franca',
          number: '980',
        },
      ];

      const mountResponse = [
        {
          id: 1,
          name: 'Hyan',
          document: '44699588745',
          user: {
            login: 'hy_campos',
            password: '12345678',
          },
          address: {
            cep: '14412410',
            street: 'Ana lucia da silva cintra',
            district: 'Centro',
            city: 'Franca',
            number: '5470',
          },
          created_at: jest
            .useFakeTimers('modern')
            .setSystemTime(new Date(2020, 9, 1, 7)),
          updated_at: jest
            .useFakeTimers('modern')
            .setSystemTime(new Date(2020, 9, 1, 7)),
        },
        {
          id: 2,
          name: 'Leonardo',
          document: '03699588745',
          user: {
            login: 'le_silva',
            password: '12345678',
          },
          address: {
            cep: '14412785',
            street: 'Av brasil',
            district: 'Jardim centenario',
            city: 'Franca',
            number: '980',
          },
          created_at: jest
            .useFakeTimers('modern')
            .setSystemTime(new Date(2020, 9, 1, 7)),
          updated_at: jest
            .useFakeTimers('modern')
            .setSystemTime(new Date(2020, 9, 1, 7)),
        },
      ];

      expect(UserResponseDTO.fromManyEntity(usersDTO)).toStrictEqual(
        mountResponse,
      );
    });
  });
});
