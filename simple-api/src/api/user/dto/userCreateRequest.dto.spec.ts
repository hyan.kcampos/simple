import { UserRequestDTO } from './userCreateRequest.dto';
import { HttpException } from '@nestjs/common';

describe('Testing UserRequest DTO', () => {
  describe('Testing loading UserRequest DTO', () => {
    it('Should create UserRequest DTO succesfully', async () => {
      const userDTO: UserRequestDTO = new UserRequestDTO();
      expect(userDTO).not.toBeNull();
    });
  });

  describe('Testing static method isValid()', () => {
    it('Validates if the object is correct', async () => {
      const userRequestDTO: UserRequestDTO = {
        id: 1,
        name: 'Hyan',
        document: '44699588745',
        login: 'hy_campos',
        password: '12345678',
        cep: '14412410',
        street: 'Ana lucia da silva cintra',
        district: 'Centro',
        city: 'Franca',
        number: '5470',
      };
      expect(UserRequestDTO.isValid(userRequestDTO)).toBe(userRequestDTO);
    });

    it('Should give error when document is null', async () => {
      try {
        const userRequestDTO: UserRequestDTO = {
          id: 1,
          name: 'Hyan',
          document: null,
          login: 'hy_campos',
          password: '12345678',
          cep: '14412410',
          street: 'Ana lucia da silva cintra',
          district: 'Centro',
          city: 'Franca',
          number: '5470',
        };
        UserRequestDTO.isValid(userRequestDTO);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });

    it('Should give error when name is null', async () => {
      try {
        const userRequestDTO: UserRequestDTO = {
          id: 1,
          name: null,
          document: '44699588745',
          login: 'hy_campos',
          password: '12345678',
          cep: '14412410',
          street: 'Ana lucia da silva cintra',
          district: 'Centro',
          city: 'Franca',
          number: '5470',
        };
        UserRequestDTO.isValid(userRequestDTO);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });

    it('Should give error when login is null', async () => {
      try {
        const userRequestDTO: UserRequestDTO = {
          id: 1,
          name: 'Hyan',
          document: '44699588745',
          login: null,
          password: '12345678',
          cep: '14412410',
          street: 'Ana lucia da silva cintra',
          district: 'Centro',
          city: 'Franca',
          number: '5470',
        };
        UserRequestDTO.isValid(userRequestDTO);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });

    it('Should give error when password is null', async () => {
      try {
        const userRequestDTO: UserRequestDTO = {
          id: 1,
          name: 'Hyan',
          document: '44699588745',
          login: 'hy_campos',
          password: null,
          cep: '14412410',
          street: 'Ana lucia da silva cintra',
          district: 'Centro',
          city: 'Franca',
          number: '5470',
        };
        UserRequestDTO.isValid(userRequestDTO);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });
  });

  describe('Testing static method toEntity()', () => {
    it('Validates if the object is correct', async () => {
      const userRequestDTO: UserRequestDTO = {
        id: 1,
        name: 'Hyan',
        document: '44699588745',
        login: 'hy_campos',
        password: '12345678',
        cep: '14412410',
        street: 'Ana lucia da silva cintra',
        district: 'Centro',
        city: 'Franca',
        number: '5470',
      };
      UserRequestDTO.toEntity(userRequestDTO);
    });
  });
});
