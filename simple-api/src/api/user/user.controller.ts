import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Get,
  Patch,
  Delete,
  Param,
  ParseIntPipe,
  Res,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserRequestDTO, UserResponseDTO, UserPatchDTO } from './dto';
import { ApiOkResponse, ApiTags, ApiResponse, ApiParam } from '@nestjs/swagger';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiOkResponse({ type: UserResponseDTO, status: HttpStatus.CREATED })
  async save(@Body() userRequestDTO: UserRequestDTO): Promise<UserResponseDTO> {
    UserRequestDTO.isValid(userRequestDTO);
    const userEntity = UserRequestDTO.toEntity(userRequestDTO);

    const response = await this.userService.saveOrUpdate(userEntity);

    return UserResponseDTO.fromOneEntity(response);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ type: UserResponseDTO, status: HttpStatus.OK })
  async get(@Res() res): Promise<UserResponseDTO[]> {
    const response = await this.userService.findAll();

    return res.status(HttpStatus.OK).json({
      status: 'success',
      records: UserResponseDTO.fromManyEntity(response)
    })
  }

  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ type: UserResponseDTO, status: HttpStatus.OK })
  @ApiParam({
    name: 'id',
    required: true,
    description: 'user id',
  })
  async findById(
    @Param('id', ParseIntPipe) id: number, @Res() res
  ): Promise<UserResponseDTO> {
    const response = await this.userService.findById(id);

    return res.status(HttpStatus.OK).json({
      status: 'success',
      records: UserResponseDTO.fromOneEntity(response)
    })
  }

  @Patch('/:id')
  @ApiResponse({ type: UserPatchDTO, status: HttpStatus.OK })
  @HttpCode(HttpStatus.OK)
  async updateUserById(
    @Param('id') userId: number,
    @Body() body: UserPatchDTO,
  ): Promise<UserResponseDTO> {
    const orders = await this.userService.updateUser(
      userId,
      UserPatchDTO.isValid(body),
    );
    return UserResponseDTO.fromOneEntity(orders);
  }

  @Delete('/:id')
  @ApiResponse({ type: UserPatchDTO, status: HttpStatus.OK })
  @HttpCode(HttpStatus.OK)
  async deleteUserById(@Param('id') userId: number): Promise<UserResponseDTO> {
    const orders = await this.userService.deleteUser(userId);
    return UserResponseDTO.fromOneEntity(orders);
  }
}
