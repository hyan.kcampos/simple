import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { UserService } from './user.service';
import * as _ from 'lodash';
import { HttpException } from '@nestjs/common';
import { ClassMock } from '../../mocks/class.mock';
import { UserRequestDTO, UserPatchDTO } from './dto';

describe('Testing User Service layer', () => {
  const mockedUser = ClassMock.CreateNewUser();
  const mockedUsers = ClassMock.CreateNewUsers();

  let userService: UserService;
  let userRepository: UserRepository;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(UserRepository),
          useValue: {},
        },
      ],
    }).compile();

    userRepository = module.get<UserRepository>(UserRepository);
    userService = new UserService(userRepository);
  });

  describe('Testing method findById()', () => {
    it('Should return a Not Found (404) error when user is not found', async () => {
      const id = 1;
      userRepository.findById = jest.fn().mockImplementationOnce(() => null);
      try {
        await userService.findById(id);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });

    it('Should return an object User when it was successfully', async () => {
      const id = 1;
      const clonedUser = _.cloneDeep(mockedUser);
      userRepository.findById = jest
        .fn()
        .mockImplementationOnce(() => clonedUser);
      const response = await userService.findById(id);
      expect(response).toMatchObject(response);
    });
  });

  describe('Testing method saveOrUpdate()', () => {
    it('Should return a user object when it was successfully saved', async () => {
      const userDTO = {
        id: mockedUser.id,
        name: mockedUser.name,
        document: mockedUser.document,
        login: mockedUser.login,
        password: mockedUser.password,
        cep: mockedUser.cep,
        street: mockedUser.street,
        district: mockedUser.district,
        city: mockedUser.city,
        number: mockedUser.number,
      } as UserRequestDTO;

      userRepository.saveOrUpdate = jest
        .fn()
        .mockImplementation(() => mockedUser);
      userRepository.find = jest.fn().mockImplementation(() => []);
      const response = await userService.saveOrUpdate(userDTO);
      expect(response).toBe(mockedUser);
    });
  });

  describe('Testing methods Hash Password', () => {
    it('Should generate a hash when it was successfully', async () => {
      const password = '123456';

      try {
        await userService.generateHashPassword(password);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });

    it('Should compare the password with hash when it was successfully', async () => {
      const password = '123456';
      const hash = 'e10adc3949ba59abbe56e057f20f883e';

      try {
        await userService.compareHashPassword(password, hash);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });
  });

  describe('Testing methods isDocumentExist()', () => {
    it('Should return a Not Found (404) error when document is not found', async () => {
      userRepository.find = jest.fn().mockImplementationOnce(() => mockedUsers);
      try {
        await userService.isDocumentExist(mockedUser);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });
  });

  describe('Testing method findAll()', () => {
    it('Should return an array User when it was successfully', async () => {
      const clonedUsers = _.cloneDeep(mockedUsers);

      userRepository.find = jest.fn().mockImplementationOnce(() => clonedUsers);

      const response = await userService.findAll();

      expect(response).toMatchObject(response);
    });
  });

  describe('Testing method patch()', () => {
    const userId = 1;
    const userPatchDTO: UserPatchDTO = {
      name: mockedUser.name,
      document: mockedUser.document,
      user: {
        login: mockedUser.login,
        password: mockedUser.password,
      },
      address: {
        cep: mockedUser.cep,
        street: mockedUser.street,
        district: mockedUser.district,
        city: mockedUser.city,
        number: mockedUser.number,
      },
    };
    it('Should return a Not Found (404) when user was not found', async () => {
      userRepository.findById = jest.fn().mockImplementationOnce(() => null);
      try {
        await userService.updateUser(userId, userPatchDTO);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });

    it('Should return a user object when it is successfully', async () => {
      const clonedUser = _.cloneDeep(mockedUser);

      userRepository.findById = jest
        .fn()
        .mockImplementationOnce(() => clonedUser);
      userRepository.saveOrUpdate = jest
        .fn()
        .mockImplementationOnce(() => clonedUser);

      const response = await userService.updateUser(userId, userPatchDTO);
      expect(response).toBe(clonedUser);
    });
  });

  describe('Testing method deleteUser()', () => {
    it('Should return a Not Found (404) error when user is not found', async () => {
      const id = 1;
      userRepository.findById = jest.fn().mockImplementationOnce(() => null);
      try {
        await userService.deleteUser(id);
      } catch (err) {
        expect(err).toBeInstanceOf(HttpException);
      }
    });

    it('Should return an object User and delete user when it was successfully', async () => {
      const id = 1;
      const clonedUser = _.cloneDeep(mockedUser);
      const mockReturnDelete = { raw: [], affected: 1 };

      userRepository.findById = jest
        .fn()
        .mockImplementationOnce(() => clonedUser);
      userRepository.delete = jest
        .fn()
        .mockImplementationOnce(() => mockReturnDelete);
      const response = await userService.deleteUser(id);
      expect(response).toMatchObject(response);
    });
  });
});
