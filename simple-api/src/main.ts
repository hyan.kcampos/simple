import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const swagger = new DocumentBuilder()
        .setTitle('Simple API')
        .setDescription('Backend to define simple api')
        .setVersion('1.0')
        .addBearerAuth({
            type: 'apiKey',
            in: 'header',
            name: 'Authorization',
            scheme: 'apiKey',
            description: 'Authorization',
            bearerFormat: 'jwt',
        })
        .build();

    const document = SwaggerModule.createDocument(app, swagger);
    SwaggerModule.setup('docs', app, document, {
        swaggerOptions: {
            defaultModelsExpandDepth: -1,
        },
    });

  await app.listen(process.env.API_PORT || 8080);
}
bootstrap();
