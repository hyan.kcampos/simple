import { AddressCommonDTO } from './';

describe('Testing Address Common DTO', () => {
  it('Should load Address Common DTO successfully', async () => {
    const addressCommonDTO: AddressCommonDTO = new AddressCommonDTO(
      '14412405',
      'Ana lucia da silva cintra',
      'Centro',
      'Franca',
      '5466',
    );
    expect(addressCommonDTO).not.toBeNull();
  });
});
