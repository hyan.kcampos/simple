import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class AddressCommonDTO {
  @ApiProperty({ example: '14402554' })
  @IsString()
  cep: string;

  @ApiProperty({ example: 'Ana lucia da silva freitas' })
  @IsString()
  street: string;

  @ApiProperty({ example: 'Centro' })
  @IsString()
  district: string;

  @ApiProperty({ example: 'Franca' })
  @IsString()
  city: string;

  @ApiProperty({ example: '5477' })
  @IsString()
  number: string;

  constructor(
    cep: string,
    street: string,
    district: string,
    city: string,
    number: string,
  ) {
    this.cep = cep;
    this.street = street;
    this.district = district;
    this.city = city;
    this.number = number;
  }
}
