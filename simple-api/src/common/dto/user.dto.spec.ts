import { UserCommonDTO } from './';

describe('Testing User Common DTO', () => {
  it('Should load User Common DTO successfully', async () => {
    const userCommonDTO: UserCommonDTO = new UserCommonDTO(
      'hy_campos',
      '12345678',
    );
    expect(userCommonDTO).not.toBeNull();
  });
});
