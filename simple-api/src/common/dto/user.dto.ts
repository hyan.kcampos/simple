import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class UserCommonDTO {
  @ApiProperty({ example: 'hy_campos' })
  @IsString()
  login: string;

  @ApiProperty({ example: '12345678' })
  @IsString()
  password: string;

  constructor(login: string, password: string) {
    this.login = login;
    this.password = password;
  }
}
